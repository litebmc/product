import os
import logging
from lbkit.tasks.task import Task

class TaskHook(Task):
    """产品定制化任务"""
    def run(self):
        pwd = os.getcwd()
        os.chdir(self.config.mnt_path)
        logging.info("Post rootfs start")
        self.exec("sed -i 's@^#KbdInteractiveAuthentication .*@KbdInteractiveAuthentication no@g' etc/ssh/sshd_config")
        self.exec("sed -i 's@^#PasswordAuthentication .*@PasswordAuthentication yes@g' etc/ssh/sshd_config")
        self.exec("rm -f etc/systemd/system/getty.target.wants/console-getty.service")
        with open("etc/passwd", "a+") as fp:
            fp.write("sshd:x:129:65534::/run/sshd:/usr/sbin/nologin\n")
            fp.write("Administrator:x:1000:1000:Linux User,,,:/home/Administrator:/bin/bash\n")
        with open("etc/shadow", "a+") as fp:
            fp.write("shd:*:19757:0:99999:7:::\n")
            fp.write("Administrator:$6$IFwKlHqLctYcRBKl$ooEpaeihd/xaEaBUnSR6VqZ4PY/vYd4d3oOvH8.Jcep94aLmn.cj78o9luqX84dNP3wVV4q6hwmgJBX6bzYv11:19885:0:99999:7:::\n")
        with open("etc/group", "a+") as fp:
            fp.write("Administrator:x:1000:\n")
        self.exec("mkdir home/Administrator -p")
        self.exec("mkdir var/empty -p")
        self.exec("chown 0:0 var/empty")
        self.exec("chown 1000:1000 home/Administrator")
        os.chdir(pwd)
