import os
import sys
import subprocess
import shutil
from lbkit.tasks.task import Task

class TaskHook(Task):
    def _prepare_sdbusplus(self, pkg):
        dep_version = pkg.split("@")[0].split("/")[1]
        installed_pkgs = subprocess.check_output([sys.executable, '-m', 'pip', 'freeze']).decode('utf-8')
        sdbusplus_pkg = "sdbusplus==" + dep_version
        if "sdbusplus" not in installed_pkgs or sdbusplus_pkg not in installed_pkgs:
            cmd = "pip3 uninstall sdbusplus"
            self.exec(cmd, ignore_error=True)
            cmd = f"pip3 install {sdbusplus_pkg}"
            self.exec(cmd)

    """产品定制化任务"""
    def run(self):
        path = os.getenv("PATH", "")
        path += ":" + os.path.expanduser("~/.local/bin")
        os.environ["PATH"] = path
        deps = self.get_manifest_config("dependencies")
        for dep in deps:
            pkg = dep.get("package")
            if not pkg.startswith("sdbusplus/"):
                continue
            self.log.info(f"sdbusplus info: {pkg}")
            self._prepare_sdbusplus(pkg)
            sdbusplus = shutil.which("sdbus++")
            if sdbusplus is None:
                raise Exception("Program 'sdbus++' not found")
