## Hardware Package Management升级包
升级工具位于litebmc/tools/make_hpm，具体用法参考make_hpm说明。
生成hpm包的命令为`make_hpm -t template.ini`

- 注: aes_pri.pem和sign_hpm.pem不应该归档在代码仓，建议由CI工程管理