# 如何构建

构建一个产品包需要完成以下步骤：
1. 安装litebmc toolkit
2. 下载产品配置
3. 构建出包

## 安装依赖

### 安装其它依赖

```bash
# rockylinux 9.x需要安装perl-core用以构建linux-pam
dnf install perl-core
# 安装epel-release
dnf install epel-release
# 依赖lbkit依赖
dnf install cmake openssl-devel zlib python3-pip git-core pkgconf glib2-devel systemd-devel lcov python3 gcc gcc-c++ kpartx fuse e2fsprogs rsync
```

### 安装lbkit

lbkit是litebmc提供的工具链，封装conan包管理器，提供组件级和产品级构建。lbkit由pip包和deb/rpm组成。
2. -toolkit为rpm或deb包，当前已构建了ubuntu的发布仓，可以使用以下命令安装

```bash
# pip包形式发布到pypi.org官方仓.
pip3 install lbkit

# deb包发布到https://litebmc.com/ubuntu制品仓，可使用apt命令安装
# 添加软件源，将下面这一行内容追加到/etc/apt/sources.list
deb [arch=amd64] https://litebmc.com/ubuntu/ stable main
# 安装gpg公钥，ID为27B8C6BEC58DCF534A7BA35E1246430E1FC586BB
wget -qO- https://litebmc.com/ubuntu/litebmc.gpg | sudo tee /etc/apt/trusted.gpg.d/litebmc.asc
# 更新索引
apt update
# 安装lbkit
apt install lbkit
```

### 安装pip包

openbmc组件构建依赖的sdbus++，需要在构建时安装，需要安装wheel和build

```bash
pip3 install build wheel
```

### 安装npm
编译openbmc的webui-vue依赖npm工具，需要更新到最新版本。
```bash
# rockylinux安装npm
dnf install npm
# ubuntu安装npm
apt install npm

# 更新node至lts版本
npm install -g n
n lts
```

## 环境配置

首先需要配置conan
```bash
# 检查perfile生成default配置
conan profile detect --force
# 添加conan组件中心仓
conan remote add litebmc https://art.litebmc.com/artifactory/api/conan/litebmc-center --force
# 登录conan组件中心仓
conan remote login -p cmVmdGtuOjAxOjE3NTg5OTM1Mzc6VHlQU0d0NngxTEszY1EwSVlEOFRzalpPZ0Qx litebmc litebmc_ro
# 配置conan
mkdir $HOME"/.conan2/cache"
echo "tools.graph:skip_binaries = False" >> ~/.conan2/global.conf
echo "core.sources:download_cache = "$HOME"/.conan2/cache" >> ~/.conan2/global.conf
echo "core.download:download_cache = "$HOME"/.conan2/cache" >> ~/.conan2/global.conf
```

## 下载产品配置

克隆`https://gitee.com/litebmc/litebmc.git`,产品配置目录依赖manifest.yml描述构建策略，包括需要使用到的工具、组件和定制化脚本。

```
git clone https://gitee.com/litebmc/litebmc.git
cd litebmc
```

## 构建出包

产品配置目录中执行`lbk build -bt minsize -pr litebmc.ini`，最终会制作出一个rootfs镜像，文件路径为`.temp/output/rootfs.img`